#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import random


# Clase motor la cual enviara parametro tipo auto.
class Motor:
    def __init__(self):
        self.motor = ""

    # Get y set para poder cambiar y obtener datos.
    def getmotor(self):
        return self.motor

    def setmotor(self, nuevomotor):
        self.motor = nuevomotor


""" Clase que servira para cambiar un objeto rueda
    que este desgastado por uno nuevo."""


class Cambiarueda:
    def __init__(self):
        self.cambiarueda = 0


""" Clase automovil que tendra todos los metodos
    que se necesitaran para crear el automovil."""


class Automovil:
    def __init__(self, motor):
        self.motor = motor
        self.estanque = 0
        self.velocimetro = 0
        self.distancia = 0
        # metodo estado sera el estado prendido y apagado.
        self.estado = 0
        self.rueda1 = 0
        self.rueda2 = 0
        self.rueda3 = 0
        self.rueda4 = 0

    # Get y set para poder cambiar y obtener datos.
    def getauto(self):
        return self.motor

    def getestanque(self):
        return self.estanque

    def getrueda1(self):
        return self.rueda1

    def getrueda2(self):
        return self.rueda2

    def getrueda3(self):
        return self.rueda3

    def getrueda4(self):
        return self.rueda4

    def getvelocimetro(self):
        return self.velocimetro

    def getestado(self):
        return self.estado

    def getdistancia(self):
        return self.distancia

    def setauto(self, nuevomotor):
        self.motor = nuevomotor

    def setestanque(self, nuevoestanque):
        self.estanque = nuevoestanque

    def setdistancia(self, nuevadistancia):
        self.distancia = nuevadistancia

    def setrueda1(self, nuevarueda1):
        self.rueda1 = nuevarueda1

    def setrueda2(self, nuevarueda2):
        self.rueda2 = nuevarueda2

    def setrueda3(self, nuevarueda3):
        self.rueda3 = nuevarueda3

    def setrueda4(self, nuevarueda4):
        self.rueda4 = nuevarueda4

    def setvelocimetro(self, nuevovelocimetro):
        self.velocimetro = nuevovelocimetro

    def setestado(self, nuevoestado):
        self.estado = nuevoestado

    # Funcion boton que hara que el automovil se pueda prender y apagar

    def boton(self):
        if auto.getestado() == 0:
            on = int(input("Presione 1 para encender el vehiculo: "))
            """ Condicion para que el usuario no
                ingrese otro numero que no sea 1."""
            while on != 1:
                on = int(input("Presione 1 para encender el vehiculo: "))
            auto.setestado(on)

            """Cada vez que se encienda el vehiculo,
                este consumira un 1% de combustible."""
            if auto.getestado() == 1:
                nuevodesgaste = auto.getestanque() * 0.99
                auto.setestanque(nuevodesgaste)

        elif auto.getestado() == 1 and auto.getestanque() > 0:
            of = int(input("Presione 0 para apagar el vehiculo: "))
            while of != 0:
                """ Condicion para que el usuario
                    no ingrese otro numero que no sea 0."""
                of = int(input("Presione 0 para apagar el vehiculo: "))
            auto.setestado(of)

    # Funcion que cambiara las ruedas que esten sobre un 90% gastadas.
    def cambioderuedas(self):
        if auto.getrueda1() > 90:
            if auto.getestado() == 0:
                """ Se cambiara el objeto de rueda 1
                    por un nuevo objeto que tendra 0% de desgaste."""
                self.rueda1 = cambiarueda.cambiarueda
                print("Rueda 1 Cambiada")

            # Pequeña condicion para apagar el auto por si esta encendido.
            if auto.getestado() == 1:
                print("Porfavor apaque el motor para cambiar la rueda")
                auto.boton()
                self.rueda1 = cambiarueda.cambiarueda
                print("Rueda 1 Cambiada")

        """Se aplicaran las mismas condiciones
            que arriba solo que con las demas ruedas."""
        if auto.getrueda2() > 90:
            if auto.getestado() == 0:
                self.rueda2 = cambiarueda.cambiarueda
                print("Rueda 2 Cambiada")

            if auto.getestado() == 1:
                print("Porfavor apaque el motor para cambiar la rueda")
                auto.boton()
                self.rueda2 = cambiarueda.cambiarueda
                print("Rueda 2 Cambiada")

        if auto.getrueda3() > 90:
            if auto.getestado() == 0:
                self.rueda3 = cambiarueda.cambiarueda
                print("Rueda 3 Cambiada")

            if auto.getestado() == 1:
                print("Porfavor apaque el motor para cambiar la rueda")
                auto.boton()
                self.rueda3 = cambiarueda.cambiarueda
                print("Rueda 3 Cambiada")

        if auto.getrueda4() > 90:
            if auto.getestado() == 0:
                self.rueda4 = cambiarueda.cambiarueda
                print("Rueda 4 Cambiada")

            if auto.getestado() == 1:
                print("Porfavor apaque el motor para cambiar la rueda")
                auto.boton()
                self.rueda4 = cambiarueda.cambiarueda
                print("Rueda 4 Cambiada")

    # Funcion que hara al auto moverse.
    def movimiento(self, distancia=0, desgaste1=0,
                   desgaste2=0, desgaste3=0, desgaste4=0):

        if auto.getestado() == 1:
            # Variable segundos que se usara despues en la formula de distancia
            segundos = random.randint(1, 10)
            velocidad = int(input("¿velocidad del vehiculo? Maximo 120 km "))

            # Condicion por si el usuario pasa el limite de velocidad
            while velocidad > 120:
                print("Quieres que te arresten?")
                velocidad = int(input("¿velocidad del vehiculo?"
                                      "Maximo 120 km "))

            """ Calculos para la formula de distancia y
                para que la distancia se vaya acumulando"""
            distanciavieja = distancia
            distancia += velocidad * (segundos / 3600)
            distancianueva = (distancia + distanciavieja)
            distanciatotal = auto.getdistancia() + distancianueva

            # Calculo para que el desgaste de las ruedas se vaya acumulando
            desgasteviejo1 = desgaste1
            desgaste1 = random.randint(1, 10)
            desgastenuevo1 = (desgaste1 + desgasteviejo1)
            desgastetotal1 = auto.getrueda1() + desgastenuevo1

            desgasteviejo2 = desgaste2
            desgaste2 = random.randint(1, 10)
            desgastenuevo2 = (desgaste2 + desgasteviejo2)
            desgastetotal2 = auto.getrueda2() + desgastenuevo2

            desgasteviejo3 = desgaste3
            desgaste3 = random.randint(1, 10)
            desgastenuevo3 = (desgaste3 + desgasteviejo3)
            desgastetotal3 = auto.getrueda3() + desgastenuevo3

            desgasteviejo4 = desgaste4
            desgaste4 = random.randint(1, 10)
            desgastenuevo4 = (desgaste4 + desgasteviejo4)
            desgastetotal4 = auto.getrueda4() + desgastenuevo4

            auto.setdistancia(distanciatotal)
            auto.setrueda1(desgastetotal1)
            auto.setrueda2(desgastetotal2)
            auto.setrueda3(desgastetotal3)
            auto.setrueda4(desgastetotal4)

            if Engine == "1.2":
                # Cuando la cilindrada sea 1.2 se consumira un litro cada 20 km
                consumoinstantaneo = (distancia - distanciavieja) / 20
                estadoestanque = auto.getestanque() - consumoinstantaneo
                auto.setestanque(estadoestanque)
                auto.setvelocimetro(velocidad)
                print("El auto ha recorrio", auto.getdistancia(), "km")
                print("Se mueve a", auto.getvelocimetro(), "km/h",)
                print("Posee", auto.getestanque(), "litros")
                print("Condicion rueda 1: ", auto.getrueda1(), "%")
                print("Condicion rueda 2: ", auto.getrueda2(), "%")
                print("Condicion rueda 3: ", auto.getrueda3(), "%")
                print("Condicion rueda 4: ", auto.getrueda4(), "%")

                """Llamado de la funcion cambio ruedas por
                   si alguna se deterioro sobre el 90%"""
                auto.cambioderuedas()

            elif Engine == "1.6":
                # Cuando la cilindrada sea 1.6 se consumira un litro cada 14 km
                consumoinstantaneo = (distancia - distanciavieja) / 14
                estadoestanque = auto.getestanque() - consumoinstantaneo
                auto.setestanque(estadoestanque)
                auto.setvelocimetro(velocidad)
                print("El auto ha recorrio", auto.getdistancia(), "km")
                print("Se mueve a", auto.getvelocimetro(), "km/h", )
                print("Posee", auto.getestanque(), "litros")
                print("Condicion rueda 1:", auto.getrueda1(), "%")
                print("Condicion rueda 2:", auto.getrueda2(), "%")
                print("Condicion rueda 3:", auto.getrueda3(), "%")
                print("Condicion rueda 4:", auto.getrueda4(), "%")
                """Llamado de la funcion cambio ruedas
                   por si alguna se deterioro sobre el 90%"""
                auto.cambioderuedas()

            # Condicion por si el estanque se queda sin gasolina
            if auto.getestanque() <= 0:
                print("El auto se quedo sin combustible este se detendra")
                auto.setestado(0)
            # Condicion por si el usuario quiere que el auto siga avanzando
            if auto.getestanque() > 0:
                continuar = input("Desea continuar? si/no ")
                if continuar.lower() == "si" and auto.getestado() == 0:
                    auto.boton()
                    return auto.movimiento()

                elif continuar.lower() == "si" and auto.getestado() == 1:
                    return auto.movimiento()

                elif continuar.lower() == "no" and auto.getestado() == 1:
                    print("Entonces se apagara el auto")
                    auto.boton()

# Llamado de funciones para que el programa funcione


decimales = ("1.2", "1.6")
Engine = random.choice(decimales)
motorsito = Motor()
auto = Automovil(motorsito)
motorsito.setmotor(Engine)
cambiarueda = Cambiarueda()
pasar = motorsito.getmotor()
auto.setauto(pasar)
print("Cilindrada: ", auto.getauto())
Deposit = 32
auto.setestanque(Deposit)
auto.boton()
auto.movimiento()
